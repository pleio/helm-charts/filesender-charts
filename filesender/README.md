# filesender

![Version: 0.1.1](https://img.shields.io/badge/Version-0.1.1-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

A Helm chart for deploying Filesender

## Source Code

* <https://gitlab.com/pleio/helm-charts/filesender-charts>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| config.admin | string | `nil` |  |
| config.database.host | string | `nil` |  |
| config.database.name | string | `nil` |  |
| config.database.password | string | `nil` |  |
| config.database.type | string | `nil` |  |
| config.database.username | string | `nil` |  |
| config.oauth.clientId | string | `nil` |  |
| config.oauth.clientSecret | string | `nil` |  |
| config.oauth.url | string | `nil` |  |
| config.oidc.clientId | string | `nil` |  |
| config.oidc.clientSecret | string | `nil` |  |
| config.oidc.url | string | `nil` |  |
| config.simplesaml.password | string | `nil` |  |
| config.simplesaml.salt | string | `nil` |  |
| config.smtp.domain | string | `nil` |  |
| config.smtp.host | string | `nil` |  |
| config.smtp.password | string | `nil` |  |
| config.smtp.tls | string | `nil` |  |
| config.smtp.username | string | `nil` |  |
| domain | string | `"filesender.foobar.tld"` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `"registry.gitlab.com/pleio/filesender"` |  |
| image.tag | string | `"latest"` |  |
| replicaCount | int | `1` |  |
| resources | object | `{}` |  |
| storage.className | string | `"efs"` |  |
| storage.name | string | `nil` |  |
| storage.storageSize | string | `"20Gi"` |  |
