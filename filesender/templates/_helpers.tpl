{{/*
Expand the name of the chart.
*/}}
{{- define "filesender.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end }}

{{- define "filesender.initName" -}}
{{- printf "%s-init" ((include "filesender.name" .)) | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "filesender.secretsName" -}}
{{- printf "%s-secrets" ((include "filesender.name" .)) | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "filesender.storageName" -}}
{{- if .Values.storage.existingDataStorage }}
{{- .Values.storage.existingDataStorage -}}
{{- else }}
{{- printf "%s-data" ((include "filesender.name" .)) | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}

{{- define "filesender.nginxConfigName" -}}
{{- printf "%s-nginx-config" ((include "filesender.name" .)) | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "filesender.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "filesender.labels" -}}
helm.sh/chart: {{ include "filesender.chart" . }}
{{ include "filesender.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "filesender.selectorLabels" -}}
app.kubernetes.io/name: {{ include "filesender.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}
